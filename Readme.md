Acenhauer
===

Acenhauer is a Acestream Live Sport Addon for XBMC/Kodi.
You can watch any sports event in HD.
It also contains an event agenda updated every 3 hours.
It needs Plexus installed and AceStream engine and player.